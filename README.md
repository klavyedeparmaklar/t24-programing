# T24 Programing Guide
## What is T24?
T24 is the core banking system developed by Temenos company.

- [T24 Programing Guide](#t24-programing-guide)
  - [What is T24?](#what-is-t24)
  - [Variables and Data Types](#variables-and-data-types)
    - [Strings](#strings)
    - [Numbers](#numbers)
    - [Dynamic Arrays](#dynamic-arrays)
    - [Dimensional Arrays](#dimensional-arrays)
  - [Arithmetic Operators <br>](#arithmetic-operators-)
  - [Comparison Operators <br>](#comparison-operators-)
  - [Control Flow](#control-flow)
    - [IF THEN ELSE](#if-then-else)
    - [BEGIN CASE END CASE](#begin-case-end-case)
    - [FOR Loop <br>](#for-loop-)
    - [WHILE LOOP](#while-loop)
  - [Compilation](#compilation)
    - [BASIC](#basic)
    - [CATALOG](#catalog)
    - [EB.COMPILE](#ebcompile)

<br>

## Variables and Data Types
### Strings 

There are 3 ways of defining strings:
```
     name = "Sebahattin Kalach"
     website = 'klavyedeparmaklar.com'
     address = \London, UK\
```
<br>

How to manipulate strings using some functions
```
     CRT LEFT(10)               ;* Sebahattin
     CRT RIGHT(6)               ;* Kalach
     CRT UPCASE(name)           ;* SEBAHATTIN KALACH
     CRT DOWNCASE("Value")      ;* value
     CRT LEN(name)              ;* 17
     CRT STR('A', 3)            ;* AAA
```
<br><br>

### Numbers
Some numerical operations.
```
     age = 24
     money = 4567.98
     CRT "Age: ":age:" money is ":money
     age ++              ;*25
     age += 1            ;*26
     age -=1             ;*25
     age =- 1            ;*-1
     CRT ISDIGIT(age)    ;*1

     * OPERATORS
     CRT 3 * 3           ;*9
     CRT 2 ** 10         ;*1024(power of 2)
     CRT 2 ^ 10          ;*1024(power of 2)
     CRT 8 / 2           ;*4
     CRT SQRT(36)        ;*6
     CRT 10 / 2 + 3       ;*8
     CRT 10 / (2 + 3)     ;*2
```

<br><br>

### Dynamic Arrays

| ASCII Desimal |      Description      |
| :-----------: | :-------------------: |
|      254      |   Field Marker(FM)   |
|      253      |   Value Marker(VM)   |
|      252      | Sub-Value Marker(SM) |

<br>

Each field is separated by a field marker and a field may contain more than one value separated by a value marker. Any value may have more than one sub-value separated by a sub-value marker.

```
     website = "klavyedeparmaklar.com"
     address = "London, UK"

     employee = "Sebahattin Kalach":@FM:address:@FM:27:@VM:website:@VM:13.31
     CRT @(-1)
     CRT employee
     PRINT employee
     PRINT FMT(employee, 'MCP')
     CRT "Address is ": employee<2> ;* The address is in the second field

     CRT @(-1)
     LOAN_REQUEST = '001-Jack-GBP-20210101-240-0.01'
     CONVERT '-' TO @FM IN LOAN_REQUEST
     CRT FMT(LOAN_REQUEST, 'MCP')
     CRT 'CUSTOMER: ': LOAN_REQUEST<2>
     CRT 'CURRENCY: ': LOAN_REQUEST<3>
```

<br><br>

### Dimensional Arrays
Dimensioned arrays use parentheses. <br> 
Arr(2, 3) <br>
2 -> Refers to the number of rows <br>
3 -> Refers to the number of columns <br>

```
     ROWS = 2
     COLS = 3

     DIM DARR(ROWS, COLS)
     DARR(1, 1) = 1 
     DARR(1, 2) = 2
     DARR(1, 3) = 3
     DARR(2, 1) = 4
     DARR(2, 2) = 5
     DARR(2, 3) = 6 

    FOR R = 1 TO ROWS
        FOR C = 1 TO COLS
            CRT @(C, R): DARR(R, C)
        NEXT C
    NEXT R

    Result is:
    1 2 3
    4 5 6    

```

<br><br>

## Arithmetic Operators <br>
Arithmetic operations are done using the following operators:
* Addition: +
* Subtraction: -
* Multiplication: *
* Division: /
* Exponent or Power: ** or ^

```
     PROGRAM ARITHMETIC.OPERATORS

     x = 2 + 3 * 4       ;* x = 14
     y = (2 + 3) * 4     ;* y = 20

  END

```

<br><br>

## Comparison Operators <br>
* EQ(Equal to) or =
* NE(Not Equal to) or <> or !
* LT(Less Than) or <
* LE(Less Than or Equal to) or <=
* GT(Greater Than) or >
* GE(Greater Than or Equal to) or >=

Some others:
* LK(Like)
* RG(Is Between)
* NR(Is Not Between)
* CT(Contains)
* NT(Does Not Contain)
* BW(Begins With)
* EW(Ends With)
* DNBW(Does Not Begin With)
* DNEW(Does Not En With)
* SAID(Sounds Like)

<br><br>

## Control Flow
Just like any other programming language, <i>INFOBASIC</i> also supports a number of control structures.

1. IF THEN ELSE
2. BEGIN CASE END CASE
3. FOR Loop
4. WHILE Loop

<br><br>

### IF THEN ELSE
```
     IF AGE <= 17 THEN
          PRINT "AGE IS LESSER THAN OR EQUAL TO 17"
          PRINT "MINOR"
     END
     ELSE
          PRINT "MAJOR"
     END

```

<br><br>

### BEGIN CASE END CASE 
```
     CUSTOMER.NAME = @LOGNAME
     BEGIN CASE
          CASE CUSTOMER.NAME = "TOM"
               DEPARTMENT.CODE = "HR"
          CASE CUSTOMER.NAME = "JACK"
               DEPARTMENT.CODE = "IT"
          CASE 1
               "DEPARTMENT NOT FOUND!"
     END CASE

```

<br></br>

### FOR Loop <br>
```
     FOR COUNTER = 1 TO 10
          CRT "TEMENOS"
     NEXT COUNTER

     Result is:
     Printed 10 times TEMENOS

```

<br><br>

### WHILE LOOP
```
     LOOP
          CRT "PLEASE ENTER 2 NUMBER: "
          INPUT I.NUM1
          INPUT I.NUM2
          WHILE I.NUM1 : I.NUM2
               CRT "TOTAL: ": I.NUM1 + I.NUM2
          REPEAT

```
<br><br>

## Compilation

### BASIC
<strong>BASIC</strong> is the command used by <i>INFOBASIC</i> to compile prorams/subroutines. It converts the code into assembly language and creates the object file. The object code has been placed under the directory BP.O automatically.

```
    BASIC MYBPFILE.BP HELLO
     
    HELLO
    BASIC_1.c
    Source file HELLO compiled successfully

```

### CATALOG
<strong>CATALOG</strong> is the command in <i>INFOBASIC</i> that makes an entry in the <b>VOC</b> for a compiled program/subroutine.

```
    CATALOG MYBPFILE.BP HELLO
     
    HELLO
    Object HELLO cataloged successfully

```
After that, you can use the routine directly by name.

```
     jsh t24 --> HELLO
     HELLO

```

### EB.COMPILE
Instead of using <b>BASIC</b> to compile and <b>CATALOG</b> to catalogue programs/subroutines we can use <strong>EB.COMPILE</strong> to compile and catalogue programs/subroutines. This command can be used in <i>JBase</i> as well to compile and catalogue.
```
    EB.COMPILE MYBPFILE.BP HELLO
     
    ...
    Source file HELLO compiled successfully
    ...
    Object HELLO cataloged successfully

```

<br><br>

<details><summary>author's details...</summary>
<p>

```
     name = "Sebahattin KALACH"
     title = 'Software Artisan'
     address = \London, UK \
```

</p>
</details>
