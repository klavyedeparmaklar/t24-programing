     PROGRAM TB.SEBO.CUST.BY.NATIONALITY

***************************************************************************************
* SUBROUTINE  : 
* REQUEST NO  :
* DATE        : 12/04/2021
* AUTHOR      : Sebahattin KALACH
* DESCRIPTION : Creating the Customer table into COMO file according to Nationality.
*
***************************************************************************************

     $INSERT I_COMMON
     $INSERT I_EQUATE
     $INSERT I_F.CUSTOMER

**************************************************************************************
*
*                       MAIN CONTROLLING SECTION
*
**************************************************************************************

     FN.FT = "F.CUSTOMER"
     F.FT  = ""
     CALL OPF(FN.FT, F.FT)

     CRLF = CHARX(13) : CHARX(10)

     TODAY = DATE()
     CRT "ENTER NATIONALITY: "
     INPUT CUST.NA
     FILE.ID = "CUSTOMER_NATIONALITY_": CUST.NA :"_" : OCONV(TODAY, "DG")

     EXECUTE "COMO ON " : FILE.ID

     NUM.OF.FT = 0
     SELECTED.FT = "SELECT " : FN.FT : " WITH NATIONALITY EQ ": CUST.NA
     CALL EB.READLIST(SELECTED.FT, FT.KEY, "", NUM.OF.FT, SYSTEM.RETURN.ERROR)
     IF NUM.OF.FT > 0 THEN
        LOOP
           REMOVE FT.ID FROM FT.KEY SETTING POS
        WHILE FT.ID:POS
           R.FT = ""
           OUT.ARRAY = ""
           READ R.FT FROM F.FT, FT.ID THEN
              OUT.ARRAY = FT.ID : '|' : R.FT
              OUT.ARRAY = EREPLACE(OUT.ARRAY, @FM, '|')
              OUT.ARRAY = EREPLACE(OUT.ARRAY, @VM, '~')
              OUT.ARRAY = EREPLACE(OUT.ARRAY, @SM, '#')
              CRT OUT.ARRAY
           END
        REPEAT
     END

     EXECUTE "COMO OFF"
  END
