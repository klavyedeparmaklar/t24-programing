     PROGRAM TB.PRACTICE

     CRT @(-1)
     PRINT "Customer ID: "
     INPUT CUST_ID

     CALL TB.PRACTICE.SUBROUTINE(CUST_ID, CUST_NAME, CUST_NATIONALITY, CUST_POSTCODE)
     CRT "Customer ID           : ": CUST_ID
     CRT "Customer Name         : ": CUST_NAME
     CRT "Customer Nationality  : ": CUST_NATIONALITY
     CRT "Customer Postcode     : ": CUST_POSTCODE

  END