     PROGRAM DIMENSIONEDARRAY

     ROWS = 2
     COLS = 3

     DIM DARR(ROWS, COLS)
     DARR(1, 1) = 1 
     DARR(1, 2) = 2
     DARR(1, 3) = 3
     DARR(2, 1) = 4
     DARR(2, 2) = 5
     DARR(2, 3) = 6 

    FOR R = 1 TO ROWS
        FOR C = 1 TO COLS
            CRT @(C, R): DARR(R, C)
        NEXT C
    NEXT R

    DIM V.VALUES(30000)            ;* size it
    MAT V.VALUES = 0               ;* assign 0 to all elements
    V.X = SYSTEM(2) - 15  ; V.Y = SYSTEM(3) - 5
    DIM V.SCREEN(V.X, V.Y)         ;* can be 2-dimensional
    V.SCREEN(1, 1) = 123           ;* here goes assignment

  END